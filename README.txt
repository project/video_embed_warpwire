Video Embed Warpwire

Video Embed Field plugin for Warpwire videos.

Dependencies:

- Video Embed Field

Installation

Install the module normally, then set the hostname on the Warpwire
settings page found under "Configuration > Media > Warpwire settings".

If your videos have URLs like https://warpwire.example.com/w/S4sCAA/,
the hostname should be set to:

https://warpwire.example.com

