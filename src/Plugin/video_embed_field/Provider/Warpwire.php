<?php

namespace Drupal\video_embed_warpwire\Plugin\video_embed_field\Provider;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\video_embed_field\ProviderPluginBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Warpwire provider plugin.
 *
 * @VideoEmbedProvider(
 *   id = "warpwire",
 *   title = @Translation("Warpwire")
 * )
 */
class Warpwire extends ProviderPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Warpwire constructor.
   *
   * @param array $configuration
   *   The configuration of the plugin.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   An HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config factory.
   *
   * @throws \Exception
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    ClientInterface $http_client,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    if (!$hostname = $this->configFactory->get('video_embed_warpwire.settings')->get('hostname')) {
      return [];
    }
    $embed_code = [
      '#type' => 'video_embed_iframe',
      '#provider' => 'warpwire',
      '#url' => sprintf($hostname . '/w/%s', $this->getVideoId()),
      '#query' => [
        'autoplay' => $autoplay,
        'start' => $this->getQueryVar('start', 0),
      ],
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
        'allow' => 'autoplay; encrypted-media; fullscreen;',
      ],
    ];
    $opional = [
      'title',
      'share',
      'info',
      'cc_load_policy',
      'seekable',
      'controls',
      'audio_only',
      'interactive_transcript',
    ];
    foreach ($opional as $variable) {
      $value = $this->getQueryVar($variable);
      if (!is_null($value)) {
        $embed_code['#query'][$variable] = $value;
      }
    } // Loop thru optional variables.
    return $embed_code;
  }

  /**
   * Get a value from the URL.
   *
   * @param string $name
   *   Parameter name.
   * @param string $default
   *   Default value.
   *
   * @return mixed|string
   *   Value of var.
   */
  protected function getQueryVar($name, $default = NULL) {
    $query = parse_url($this->getInput(), PHP_URL_QUERY);
    $variables = [];
    parse_str($query, $variables);
    return $variables[$name] ?? $default;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    $embed_data = $this->oEmbedData();
    return $embed_data->thumbnail_url ?? NULL;
  }

  /**
   * Get the Warpwire oembed data.
   *
   * @return array
   *   An array of data from the oembed endpoint.
   */
  protected function oEmbedData() {
    if (!$hostname = $this->configFactory->get('video_embed_warpwire.settings')->get('hostname')) {
      return NULL;
    }
    return json_decode(file_get_contents($hostname . '/api/oembed?url=' . $this->getInput()));
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    if (!$hostname = \Drupal::config('video_embed_warpwire.settings')->get('hostname')) {
      return FALSE;
    }
    preg_match('~^' . preg_quote($hostname, '~') . '/w/(?<id>[0-9A-Za-z_-]*)~', $input, $matches);
    return $matches['id'] ?? FALSE;
  }

}
