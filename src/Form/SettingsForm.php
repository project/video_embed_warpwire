<?php

namespace Drupal\video_embed_warpwire\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Warpwire Video Embed settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'video_embed_warpwire_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['video_embed_warpwire.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname'),
      '#description' => $this->t('The full hostname for Warpwire videos (e.g. <em>https://warpwire.example.com</em>).'),
      '#default_value' => $this->config('video_embed_warpwire.settings')->get('hostname'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($hostname = $form_state->getValue('hostname')) {
      if (!preg_match('~^https?://(.+)~', $hostname)) {
        $form_state->setErrorByName('hostname', $this->t('Hostname must be of the format <em>https://warpwire.example.com</em>.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('video_embed_warpwire.settings')
      ->set('hostname', $form_state->getValue('hostname'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
